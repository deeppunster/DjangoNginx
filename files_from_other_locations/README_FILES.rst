###################################
Files Copied From Other Directories
###################################

From /etc/uwsgi/sites/
======================

sample.ini
----------

This file contains configuration information for uWSGI.  How much is actually
used is unknown at this time.  

From /usr/local/etc/nginx/
==========================

nginx.conf
----------

This is the main configuration file for nginx.  It contains a referance to
the uwsgi_params file mentioned next.

uwsgi_params
------------

This contains default parameters for managing uWSGI.
