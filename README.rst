###############################################################
Django and Nginx with Docker Tutorial
###############################################################

These notes were tested on MacOS using brew to install additional
system packages.

*********************
Preinstallation Notes
*********************

Pyenv
=====

Environment
-----------

-   Already installed via Homebrew
-		Also presinstalled was pyenv-virtualenv

Investigation
-------------

No man page, pyenv --help was minimal.

Went to `pyenv Tutorial <https://amaral.northwestern.edu/resources/guides/pyenv-tutorial>`_
for help.

Suggested items:

-	run ```pyenv global```

	-	Result was just "system"
				
Install support for the various version of Python 3 I have installed.

```
		pyenv install 3.7.7
		
		pyenv install 3.8.3
```

Apparently it does not use any of the versions I have installed.  Instead, it 
downloads and installs a fresh build from its own resources.  Run::

		pyenv install -list
		
to see all possible versions it can install.

To set a default global version use::

    pyenv global <version>  
    
where <version> can either be a specific version installed above (e.g. 3.8.3) 
or "system" which means the default python found in the PATH environment 
variable.

Implement pyenv
^^^^^^^^^^^^^^^

1.  Install pyenv via brew.
2.  Install pyenv-virtualenv via brew.
3.  Modify .bash-profile using the instructions at 
    `https://github.com/pyenv/pyenv#understanding-shims`_ and scroll down to 
    "Choosing the Python Version".  It suggests running ```python shell``` 
    and following the instructions. 
    
Move to Project Home
====================

Move into the project's home directory::

    cd <project directory>

    
Create Virtual Environment for Project
======================================

Use pyenv to create standard virtualenv-style venv directory, according to
`https://amaral.northwestern.edu/resources/guides/pyenv-tutorial`_

::

    pyenv virtualenv 3.8.3 venv

    
Alternate Virtual Environment Creation
--------------------------------------

::

    python3 -m venv venv

    
Prepare Virtual Environment
===========================

Slip into virtual environment::

    source venv/bin/activate
    
Update pip and setuptools, and add wheel::

    pip install -U pip setuptools
    pip install wheel

    
Install Requirements for Project
================================

Install system tools::

    brew install nginx sqlite docker-machine uwsgi
    brew cask install docker
    
Note - installing docker also installed docker-compose.

Install Python libraries.
    
    pip install Django virtualenv virtualenvwrapper uwsgi
    
Prepare shell for virtualenvwrapper (which modified user .bashrc file)::

    echo "export WORKON_HOME=~/Env" >> ~/.bashrc
    echo "[ `which virtualenvwrapper.sh` ] && source virtualenvwrapper.sh" >> ~/.bashrc
    source ~/.bashrc
    
Note - modified the second line above to find the shell script 
```virtualenvwrapper.sh``` wherever it is located and to not complain 
if it is not found.
    
Build Django Project
====================

Create Django project framework::

    django-admin.py startproject sample 
    
At this point the Django project -- called sample -- is in the subdirectory 
of the project directory and that directory is also called sample.  The 
directory structure looks like this::

    <Project Directory>
    |  
    --sample  - Django container
      |
      - manage.py
      |
      - sample  - Django project
        |
        - settings.py
        - asgi.py
        - wsgi.py
        - urls.py

Go to the Django container directory and run the following to populate the 
sqlite database with a schema and some data in the auth tables::

    ./manage.py migrate

Modify the settings.py file to establish a location for all static files.  
This is required to use nginx. Add this line to the settings.py file. ::

    STATIC_ROOT = os.path.join(BASE_DIR, "static/")
    
Run the following to locate all static files and put copies in the static 
directory in the same relative position.  (The static directory will be 
created to contain the copies.) ::

    ./manage.py collectstatic
    
Verify that Django is minimally set up. ::

    ./manage.py runserver localhost:8000
    
Open a browser to http://localhost:8000/ to see the default Django page.

Stop the server with a ^C.

Configure uWSGI
===============

Create a directory to hold the uWSGI configuration files. ::

    sudo mkdir -p /etc/uwsgi/sites
    
Added file to that directory called sample.ini.  It coantains::

    [uwsgi]
    project = sample
    base = /home/django
    
    chdir = %(base)/%(project)
    home = %(base)/Env/%(project)
    module = %(project).wsgi:application
    
    master = true
    processes = 2
    
    socket = %(base)/%(project)/%(project).sock
    chmod-socket = 664
    vacuum = true

At this point the tutorial stated to create an Upstart job.  Upstart is a 
package only available for Linux distros.  It replaces the system init 
processing.  Thus we need to test uWSGI some other way.

Using the documentation at 
`https://uwsgi-docs.readthedocs.io/en/latest/WSGIquickstart.html`_ create 
a file in the project directory called ```uwsgi_hello_world.py```.  In it 
add the following::

    def application(env, start_response):
        start_response('200 OK', [('Content-Type','text/html')])
        return [b"Hello World"]
        
Run this command in the project directory::

    uwsgi --http :9090 --wsgi-file uwsgi_hello_world.py
    
From a browser enter the url `http://localhost:9090/`_.  If all is well 
the browser should display a simple "Hello World".  Use ^C to stop uwsgi.

Configure nginx
===============

Brew puts the configuration files in ```/usr/local/etc/nginx```.  Add or 
modify a file in that directory called ```nginx.conf``` to contain::

    
    worker_processes 2;
    
    error_log  logs/error.log;
    error_log  logs/error.log  notice;
    error_log  logs/error.log  info;
    
    pid        logs/nginx.pid;
    
    events {
        worker_connections  256;
    }
    
    http {
        include       mime.types;
        default_type  application/octet-stream;
    
        log_format  main  '$remote_addr - $remote_user [$time_local] "$request" | '
                          '$status | $body_bytes_sent | "$http_referer" | '
                          '"$http_user_agent" | "$http_x_forwarded_for"';
    
        access_log  logs/access.log  main;
    
        sendfile        on;
        #tcp_nopush     on;
    
        #keepalive_timeout  0;
        keepalive_timeout  65;
    
        #gzip  on;
    
    	server {
    		listen 127.0.0.1:8080;
            server_name  localhost;
    
            access_log  logs/host.access.log  main;
    
            location / {
                include uwsgi_params;
                uwsgi_pass 127.0.0.1:8765;
            }
    
    	}
    }

This configuration states that nginx should listen for traffic on 
localhost at port 8080.

Start nginx::

    nginx
    
Verify that it is running::

    sudo -C nmap localhost -p 8000-9000
    
This should report that a process is listening at port 8080.

A browser trying the url ```http://localhost:8080/``` should get a 
```502 Bad Gateway``` error.

Nginx and uWSGI Together
========================

Start uWSGI (again in the project directory) with this command::

    uwsgi --socket 127.0.0.1:8765 --wsgi-file uwsgi_hello_world.py --master --processes 4 --threads 2 --stats 127.0.0.1:8700
    
A browser trying the url ```http://localhost:8080/``` should get  
```Hello World``` again.

Kill uwsgi with ^C.

Django, Nginx and uWSGI
=======================

Switch to the <Project Directory>/sample directory.

Start uwsgi up again, but point to the wsgi.py file in the Django Project 
directory::

    uwsgi --socket 127.0.0.1:8765 --wsgi-file sample/wsgi.py --master --processes 4 --threads 2 --stats 127.0.0.1:8700
    
Try the url ```http://localhost:8080/``` in a browser.  The django startup 
page should now be displayed.

 